﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {

    public int value;
    public float timeToDie;
    public Animator coinAnimator;

    private void Start()
    {
        //TODO start counter in order to make disapear the coin 5seg?
        InvokeRepeating("CoinLiveCounter", 1, 1);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            CoinTaken();
        }
    }

    private void CoinTaken()
    {
        DestroyCoin();
    }

    private void CoinLiveCounter()
    {
        timeToDie--;
        if(timeToDie == 0)
        {
            CancelInvoke();
            coinAnimator.SetTrigger("TimeOut");
        }
    }

    private void DestroyCoin()
    {
        Destroy(gameObject);
    }
}
