﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject cameraParent;
    public GameObject hitIndicator;
    public float playerSpeed;
    public GameObject weaponPivot;
    public delegate void EnemyHit();
    public event EnemyHit decreaseLive;
    public float dashSpeed;
    public AudioClip clip;
    [HideInInspector]
    public bool isDead;
    [HideInInspector]
    public float damagePerHit;
    [HideInInspector]
    public bool canDash;

    private Rigidbody2D playerRb;
    private Vector2 moveVelocity;
    private Animator playerAnim;
    private AudioSource audioSource;
    private Animator hitIndicatorAnim;
    private Animator cameraAnimator;


	void Start ()
    {
        cameraAnimator = cameraParent.GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
        playerRb = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();
        hitIndicatorAnim = hitIndicator.GetComponent<Animator>();
        damagePerHit = 30f;
        canDash = false;
        isDead = false;
	}

    private void FixedUpdate()
    {
        if(!isDead)
        {
            Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            //Normalize value in order to make diagonal movement with normal speed as horizontal and vertical
            if (moveInput != new Vector2())
            {
                playerAnim.SetBool("walking", true);
            }
            else
            {
                playerAnim.SetBool("walking", false);
            }
            moveVelocity = moveInput.normalized * playerSpeed;
            CheckMousePosition();
            CheckDashAction(moveInput);
            playerRb.MovePosition(playerRb.position + moveVelocity * Time.fixedDeltaTime);
        }
    }
    /// <summary>
    /// AIM with the weapon facing mouse position
    /// </summary>
    void CheckMousePosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector3 direction = new Vector2(mousePosition.x - weaponPivot.transform.position.x, mousePosition.y - weaponPivot.transform.position.y);
        weaponPivot.transform.up = direction;
    }

    /// <summary>
    /// IF tha player has bought the dash hability it can be activated
    /// </summary>
    /// <param name="playerMovement"></param>
    private void CheckDashAction(Vector2 playerMovement)
    {
        var moveVelocity = playerMovement.normalized * dashSpeed;
        if(canDash)
        {
            if(Input.GetMouseButtonDown(1) )
            {
                playerRb.AddForce(moveVelocity * dashSpeed);
            }
        }
    }

    /// <summary>
    /// Created in order to manage coin collision
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Detect when the player takes a coin
        if(other.CompareTag("Coin"))
        {
            var coinValue = other.gameObject.GetComponent<CoinManager>().value;
            GamePlayManager.coins += coinValue;
        }
        if(other.CompareTag("Enemy") || other.CompareTag("EnemySon"))
        {
            //Maybe this 2 lines should be placed on TakeDamage
            audioSource.clip = clip;
            audioSource.Play();
            //
            //TODO add camera animation
            cameraAnimator.SetTrigger("shake");
            //
            other.GetComponent<EnemyController>().TakeDamage(damagePerHit);
        }
    }

    /// <summary>
    /// Detects the collisions from enemies to player body
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Enemy") || other.collider.CompareTag("Bullet") || other.collider.CompareTag("EnemySon"))
        {
            //Detect when the player body is hit by an enemy
            if(other.contacts[0].otherCollider.GetComponent<Collider2D>().CompareTag("Player"))
            {
                if (decreaseLive != null)
                {
                    hitIndicatorAnim.SetTrigger("damage");
                    decreaseLive();
                }
            }
        }
    }
}
