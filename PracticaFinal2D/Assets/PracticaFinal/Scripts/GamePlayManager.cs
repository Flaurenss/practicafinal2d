﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayManager : MonoBehaviour {

    public PlayerController player;
    public Text coinCounter;
    public Text livesCounter;
    public Text enemiesCounter;
    public Text finalEnemiesCounter;
    public int enemiesKilled;
    public static int coins;
    public static int lives;
    public GameObject deathScreen;

    private void Start()
    {
        enemiesCounter.text = (enemiesKilled = 0).ToString();
        coinCounter.text = (coins = 0).ToString();
        livesCounter.text = (lives = 5).ToString();
        player.decreaseLive += DecreaseLive;
    }

    private void Update()
    {
        SetCanvasValues();
        if(lives <= 0)
        {
            Die();
        }
    }

    public void SetCanvasValues()
    {
        enemiesCounter.text = enemiesKilled.ToString();
        coinCounter.text = coins.ToString();
        livesCounter.text = lives.ToString();
    }

    public void DecreaseLive()
    {
        lives --;
    }

    /// <summary>
    /// When reach 0 lives, game is over
    /// </summary>
    private void Die()
    {
        finalEnemiesCounter.text = enemiesKilled.ToString();
        gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        gameObject.GetComponent<PlayerController>().weaponPivot.SetActive(false);
        gameObject.GetComponent<PlayerController>().isDead = true;
        deathScreen.SetActive(true);
        deathScreen.GetComponent<Animator>().SetTrigger("dead");
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
