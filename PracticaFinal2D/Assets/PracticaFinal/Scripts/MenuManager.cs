﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public GameObject credits;

    public void GoToMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void DisplayCredits()
    {
        //Activate image with credits
        if(credits.activeSelf)
        {
            credits.SetActive(false);
        }
        else
        {
            //Activate credits
            credits.SetActive(true);
        }
    }

    public void ExitGame()
    {
        Application.Quit();

    }
}

