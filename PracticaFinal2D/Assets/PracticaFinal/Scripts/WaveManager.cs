﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour {
    public GameObject player;
    public GameObject melee;
    public GameObject distance;
    public GameObject ball;
    public GameObject mother;
    public Transform[] spawnPoints;
    public float startTimeBtwSpawns;
    public int initialEnemies;
    [HideInInspector]
    public int numEnemies;
    //Images from skills
    public Sprite healthCard;
    public Sprite damageCard;
    public Sprite speedCard;
    public Sprite dashCard;
    public Sprite weapon1Card;
    public Sprite weapon2Card;
    public Sprite weapon3Card;
    public GameObject skillBackground;
    public Transform skillDeckTransform;
    public GameObject skillButton;
    public GameObject midWeapon;
    public GameObject ultraWeapon;

    private float timeBtwSpawns;
    //The increment number of enemies after finishing a wave
    private int newEnemies;
    private int waveEnemies;
    private bool wave;
    private List<RandomObject> enemiesList;
    private List<SkillCreator.Skill> skillList;
    private List<SkillCreator.Skill> skillDeckList;
    private float slowDownFactor = 4f;
    private PlayerController playerController;

    private void Start()
    {
        SlowMotion(false);
        skillDeckList = new List<SkillCreator.Skill>();
        playerController = player.GetComponent<PlayerController>();
        SetEnemiesProbability();
        wave = true;
        timeBtwSpawns = startTimeBtwSpawns;
        skillList = SkillsCreator();
        waveEnemies = initialEnemies + newEnemies;
    }

    private void Update()
    {
        if(wave)
        {
            if(timeBtwSpawns <= 0)
            {
                //Select random spawnpoint in order to spawn the enemy
                int randSpawnPoint = Random.Range(0, spawnPoints.Length);
                SpawnEnemy(randSpawnPoint);
                waveEnemies--;
                numEnemies++;
                timeBtwSpawns = startTimeBtwSpawns;
                if (waveEnemies <= 0)
                {
                    wave = false;
                }
            }
            else
            {
                timeBtwSpawns -= Time.deltaTime;
            }
        }
        if (numEnemies == 0 && !wave)
        {
            newEnemies += 2;
            waveEnemies = initialEnemies + newEnemies;
            wave = true;
            skillBackground.SetActive(true);
            DestroySkillDeck();
            GetRandomSkills();
            SlowMotion(true);
        }
    }
    /// <summary>
    /// Assign spawn probabilites to each kind of enemies
    /// </summary>
    private void SetEnemiesProbability()
    {
        enemiesList = new List<RandomObject>
        {
            new RandomObject(mother, 5),
            new RandomObject(ball, 10),
            new RandomObject(distance, 40),
            new RandomObject(melee, 45),
        };
    }

    private void SpawnEnemy(int spawnPoint)
    {
        var randomValue = Random.Range(0,100);
        GameObject enemyToSpawn = null;
        double comulative = 0.0f;
        for(int i =0; i< enemiesList.Count; i++)
        {
            comulative += enemiesList[i].probability;
            if(comulative >= randomValue)
            {
                enemyToSpawn = enemiesList[i].newObject;
                break;
            }
        }
        Instantiate(enemyToSpawn, spawnPoints[spawnPoint].position, Quaternion.identity);
    }

    private List<SkillCreator.Skill> SkillsCreator()
    {
        List<SkillCreator.Skill> skillsList = new List<SkillCreator.Skill>();

        var skill1 = SkillCreator.CreateSkill(SkillCreator.SkillType.SpecialSkill, false, 1, 10, 10, dashCard);
        skillsList.Add(skill1);
        var skill2 = SkillCreator.CreateSkill(SkillCreator.SkillType.WeaponType1, false, 1, 10, 30, weapon1Card);
        skillsList.Add(skill2);
        var skill3 = SkillCreator.CreateSkill(SkillCreator.SkillType.WeaponType2, false, 1, 10, 50, weapon2Card);
        skillsList.Add(skill3);
        var skill4 = SkillCreator.CreateSkill(SkillCreator.SkillType.WeaponType3, false, 1, 10, 60, weapon3Card);
        skillsList.Add(skill4);
        var skill5 = SkillCreator.CreateSkill(SkillCreator.SkillType.Health, true, 0, 20, 20, healthCard);
        skillsList.Add(skill5);
        var skill6 = SkillCreator.CreateSkill(SkillCreator.SkillType.Damage, true, 0, 20, 20, damageCard);
        skillsList.Add(skill6);
        var skill7 = SkillCreator.CreateSkill(SkillCreator.SkillType.Speed, true, 0, 20, 10, speedCard);
        skillsList.Add(skill7);
        return skillsList;
    }

    private void GetRandomSkills()
    {
        skillDeckList.Clear();
        int deckLimit = 0;
        while(deckLimit != 3)
        {
            int randomValue = Random.Range(0,101);
            SkillCreator.Skill skill = null;
            int comulative = 0;
            for(int e = 0; e < skillList.Count; e++)
            {
                comulative += skillList[e].percentage;
                if(comulative >= randomValue)
                {
                    if(skillDeckList == null || !skillDeckList.Contains(skillList[e]))
                    {
                        if(skillList[e].infinite)
                        {
                            skill = skillList[e];
                            break;
                        }
                        else
                        {
                            if(skillList[e].numAppearances > 0)
                            {
                                skill = skillList[e];
                                skillList[e].numAppearances--;
                                break;
                            }
                        }
                    }
                }
            }
            if(skill != null)
            {
                skillDeckList.Add(skill);
                deckLimit++;
            }
        }

        DestroySkillDeck();
        foreach(SkillCreator.Skill skill in skillDeckList)
        {
            GameObject skillButtonClone = Instantiate(skillButton);
            skillButtonClone.transform.SetParent(skillDeckTransform, false);
            skillButtonClone.GetComponent<Image>().sprite = skill.representativeImage;
            skillButtonClone.GetComponentInChildren<Text>().text = skill.price.ToString();
            FillListener(skillButtonClone.GetComponent<Button>(), skill);
        }
    }

    private void FillListener(Button button, SkillCreator.Skill skill)
    {
        button.onClick.AddListener(() =>
        {
            ActivateSkill(skill);
        });
    }

    private void ActivateSkill(SkillCreator.Skill skill)
    {
        if(skill.price <= GamePlayManager.coins)
        {
            QuitSkillDeck();
            GamePlayManager.coins -= (int)skill.price;
            CheckSkillSelected(skill);
        }
    }

    private void SlowMotion(bool power)
    {
        if(power)
        {
            //Activate slow motion
            Time.timeScale = Time.timeScale/slowDownFactor;
            //Reduce this in order to get a properly smooth simulation
            Time.fixedDeltaTime = Time.timeScale * .02f;
        }
        else
        {
            //Desactivate slow motion
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02f;
        }
    }

    //Erase random actual skills from canvas
    private void DestroySkillDeck()
    {
        foreach(Transform child in skillDeckTransform)
        {
            Destroy(child.gameObject);
        }
    }
    
    public void QuitSkillDeck()
    {
        skillBackground.SetActive(false);
        SlowMotion(false);
    }

    private void CheckSkillSelected(SkillCreator.Skill skill)
    {
        switch(skill.type)
        {
            case SkillCreator.SkillType.Health:
                GamePlayManager.lives++;
                break;
            case SkillCreator.SkillType.Damage:
                playerController.damagePerHit += 2;
                break;
            case SkillCreator.SkillType.Speed:
                playerController.playerSpeed+= .5f;
                break;
            case SkillCreator.SkillType.SpecialSkill:
                playerController.canDash = true;
                break;
            case SkillCreator.SkillType.WeaponType1:
                playerController.damagePerHit += 20;
                break;
            case SkillCreator.SkillType.WeaponType2:
                //Change actual weapon to midweapon
                ChangePlayerWeapon(midWeapon);
                break;
            case SkillCreator.SkillType.WeaponType3:
                //Change actual weapon to final weapon
                ChangePlayerWeapon(ultraWeapon);
                break;
        }
    }

    private void ChangePlayerWeapon(GameObject newWeapon)
    {
        foreach(Transform child in playerController.weaponPivot.transform)
        {
            Destroy(child.gameObject);
        }
        
        GameObject weaponClone = Instantiate(newWeapon, playerController.weaponPivot.transform.position, playerController.weaponPivot.transform.rotation);
        weaponClone.transform.SetParent(playerController.weaponPivot.transform, true);
    }
}
