﻿using UnityEngine;
/// <summary>
/// Relates a certain gameobject with its proability to appear on the game scene
/// </summary>
public class RandomObject {

    public GameObject newObject;
    public double probability;

    public RandomObject(GameObject newObject, double probability)
    {
        this.newObject = newObject;
        this.probability = probability;
    }
}
