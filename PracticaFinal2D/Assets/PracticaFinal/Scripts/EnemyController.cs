﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour {

    private Transform target;
    private Rigidbody2D enemyRb;
    public enum EnemyType { Melee, Distance, Balloon, Mother };
    public EnemyType enemyType;
    [HideInInspector]
    public float enemyMoveSpeed;
    public float shootingDistance;
    public GameObject bullet;
    public GameObject sonEnemy;
    public GameObject spawnPoint;
    public int coinValue;
    public GameObject coinPrefab;
    public float health;

    private Vector3 direction;
    private bool changeDirection;
    private float timeBtwSpawn;
    public float startTimeBtwSpawn;
    private GamePlayManager gameManager;
    private bool isAlive;
    private WaveManager waveManager;

    // Use this for initialization
    void Start ()
    {
        isAlive = true;
        changeDirection = false;
        enemyRb = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        gameManager = target.gameObject.GetComponent<GamePlayManager>();
        SetEnemiesParameters();
        waveManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<WaveManager>();

    }
    /// <summary>
    /// All enemy movements are controled on FixedUpdate cause it's suppoused to be more consistent to the gameplay
    /// </summary>
    private void FixedUpdate()
    {
        switch (enemyType)
        {
            case EnemyType.Melee:
                TargetChase();
                break;
            case EnemyType.Distance:
                ShooterChase();
                break;
            case EnemyType.Balloon:
                BalloonTrajectory();
                break;
            case EnemyType.Mother:
                MotherChase();
                break;
            default:
                break;
        }
        if(health <= 0)
        {
            Die();
        }
    }

    private void TargetChase()
    {
        //Basic chase to player
        transform.position = Vector2.MoveTowards(transform.position, target.position, enemyMoveSpeed * Time.fixedDeltaTime);
        //Debug.DrawLine(transform.position, target.position, Color.green);
    }

    private void ShooterChase()
    {
        TargetChase();
        //If reachs the min dsitance the enemy will shoot 
        var distance = Vector2.Distance(transform.position, target.position);
        if (distance < shootingDistance)
        {
            if(timeBtwSpawn <= 0)
            {
                //Spawn bullet on enemy position with no rotation
                Instantiate(bullet, transform.position, Quaternion.identity);
                timeBtwSpawn = startTimeBtwSpawn;
            }
            else
            {
                timeBtwSpawn -= Time.fixedDeltaTime;
            }
        }
    }

    private void BalloonTrajectory()
    {
        //In case enemy collide to other enemy, will change direction to its opposite
        if (changeDirection)
        {
            direction = -direction;
        }
        Vector3 newPos = transform.position + direction * enemyMoveSpeed * Time.fixedDeltaTime;
        enemyRb.MovePosition(newPos);
        changeDirection = false;
    }

    private void MotherChase()
    {
        Vector3 dir = target.position - spawnPoint.transform.position;
        dir = spawnPoint.transform.InverseTransformDirection(dir);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        spawnPoint.transform.RotateAround(transform.position, Vector3.forward, angle - 90);
        if (timeBtwSpawn <= 0)
        {
            Instantiate(sonEnemy, spawnPoint.transform.position, Quaternion.identity);
            timeBtwSpawn = startTimeBtwSpawn;
        }
        else
        {
            timeBtwSpawn -= Time.fixedDeltaTime;
        }
        TargetChase();
    }

    private void SetEnemiesParameters()
    {
        switch(enemyType)
        {
            case EnemyType.Melee:
                SetColliderInvisible();
                enemyMoveSpeed = Random.Range(2.7f, 4f);
                break;
            case EnemyType.Distance:
                SetColliderInvisible();
                enemyMoveSpeed = 2.5f;
                timeBtwSpawn = startTimeBtwSpawn;
                break;
            case EnemyType.Balloon:
                var directionRnd = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                //Normalize the direction in order to avoid the slowing speed issue
                direction = directionRnd.normalized;
                enemyMoveSpeed = 5f;
            break;
            case EnemyType.Mother:
                SetColliderInvisible();
                enemyMoveSpeed = 0.5f;
            break;
            default:
            break;
        }
    }

    /// <summary>
    /// Ignore collision between security area of enemies (in order to have a certain dsitance) and the body of the player/target
    /// </summary>
    private void SetColliderInvisible()
    {
        Physics2D.IgnoreCollision(GetComponentInChildren<CircleCollider2D>(), target.GetComponent<BoxCollider2D>());
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //If the enemy collides to the player will die
        if(other.collider.CompareTag("Player") && isAlive)
        {
            Die();
        }
        if (enemyType == EnemyType.Balloon && other.gameObject.tag == "WallLimit")
        {
            //Get the direction where the enemy is colliding
            direction = other.GetContact(0).normal;
            //Get a random angle rotation in order to get the new direction but normalized in order to avoid the slowing issue
            //and creates a rotation in order to avoid the gameobject to get stuck to the wall.
            direction = (Quaternion.AngleAxis(Random.Range(-70.0f, 70.0f), Vector3.forward) * direction).normalized;
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
    }

    private void Die()
    {
        isAlive = false;
        //Increase enemies killed number
        gameManager.enemiesKilled += 1;
        //Drop coin with specific value depending on enemy killed and depending on luck
        if(Random.value > 0.5)
        {
            GameObject coin = Instantiate(coinPrefab, transform.position, Quaternion.identity);
            coin.GetComponent<CoinManager>().value = coinValue;
        }
        if(!gameObject.CompareTag("EnemySon"))
        {
            waveManager.numEnemies--;
        }
        //Destroy enemy      
        Destroy(gameObject);
    }
}
