﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Class that allows the creation of a specific skill depending on the SkillType(expandable)
/// </summary>
public class SkillCreator{

    public enum SkillType { Health, Damage, WeaponType1, WeaponType2, WeaponType3, SpecialSkill, Speed };

    public class Skill
    {
        public SkillType type;
        public bool infinite;
        public int numAppearances;
        public int percentage;
        public double price;
        public Sprite representativeImage;
    }

    public static Skill CreateSkill(SkillType type, bool infinite,int numAppearances, int percentage, double price, Sprite representativeImage)
    {
        Skill skill = new Skill
        {
            type = type,
            infinite = infinite,
            numAppearances = numAppearances,
            percentage = percentage,
            price = price,
            representativeImage = representativeImage
        };

        return skill;
    }
}
