﻿using UnityEngine;

public class BulletController : MonoBehaviour {

    public float bulletSpeed;

    private Transform player;
    private Vector3 direction;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        direction = player.position - transform.position;
        direction = direction.normalized;
    }

    private void FixedUpdate()
    {
        transform.position += direction * bulletSpeed* Time.fixedDeltaTime;
        Debug.DrawLine(transform.position, direction, Color.green);
    }

    private void DestroyBullet()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            DestroyBullet();
        }
        else if (other.collider.CompareTag("WallLimit"))
        {
            DestroyBullet();
        }
    }
}
