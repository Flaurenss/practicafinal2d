﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalSpin : MonoBehaviour {

    //TODO test different velocities
    private float rotateSpeed = 3.5f;

    // Update is called once per frame
	void Update ()
    {
        //TODO rotate 360 the portal
        transform.Rotate(Vector3.forward * rotateSpeed);
	}
}
