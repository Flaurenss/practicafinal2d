﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;

    private float zPosition;

    private void Start()
    {
        zPosition = -1f;
    }

    void LateUpdate ()
    {
        transform.position = new Vector3(target.position.x, target.position.y, zPosition);
	}
}
