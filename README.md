# Introduction

Arena King is a survival and combat game in real time. The goal of the player is to survive as long as possible, while facing waves of enemies.

## Game instructions

As previously mentioned, the goal of the player is to bring down as many enemies as possible (surviving the maximum possible time).
The player can move through the arena using the arrow keys or the WASD keys. The attacks of this are based on the movement of the mouse as well as the use of the right click of the mouse for the use (not available at the beginning of the game) of special abilities.
When a specific number of enemies have been shot down, the game goes into slowmotion and comes out in the middle of the screen, a series of skills that the user can buy with money. Such money is obtained with certain probabilities, by killing enemies.

## Dev information

On this occasion I have been in charge of developing the art of the game on my own. The idea of the project was to implement a survival game where the player should overcome different rounds.  The game begins the confrontation against two enemies, once dejected two more enemies are added to the total of enemies of the round and so on.

The central points of the project reside in the EnemyController class, which is responsible for defining the behaviors of different types of enemies; each enemy has different properties and its common core is the fact that they persecute the user; It remains to implement this class through the Factory pattern.
Each enemy to be shot down has a percentage of dropping a certain amount of gold (specific according to the type of enemy we beat) and each enemy is assigned a certain probability of appearing in a round.
On the other hand, we have the WaveManager class which is responsible for managing the skills that are shown to the user between rounds (3 skills will be shown). There are currently a total of seven different skills, each with a different percentage of appearance. Currently, and more specifically on line 171, there is some kind of incidence in the skill selection process when the sum of their percentages does not match the random number generated to obtain that ability. There is currently a fix applied so it should not have an impact on the gameplay.

There are some features that will be included in the future:

- Death particle system on enemy death.
- Music for the game.
- Menu more sofisticated.
-...





